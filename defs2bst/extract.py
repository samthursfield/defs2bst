#!/usr/bin/env python3
#
#  Copyright (C) 2017 Codethink Limited
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Tristan Van Berkom <tristan.vanberkom@codethink.co.uk>

import os
import shutil
from . import run_program
from . import timed_activity
from . import chdir

def extract(ybd, defsdir, workdir, target, arch):
    """Use ybd to extract the summary yaml for a given target

    Args:
       ybd (str): Path to the ybd.py program
       defsdir (str): Path to definitions repository to update
       workdir (str): Location to store the resulting file
       target (str): defsdir directory relative YBD target morph
       arch (str): Target architecture for YBD

    Returns:
       (str): The path to the extracted YAML file

    This will use YBD to extract the yaml file we want, the yaml
    file will be stored in workdir and any supplementary clutter
    which YBD creates will be removed from defsdir.
    """
    resultname = os.path.splitext(os.path.basename(target))[0] + '.yml'

    with timed_activity("Extracting {} from definitions at {}".format(resultname, defsdir)):
        with chdir(defsdir):
            run_program([
                ybd,
                '--mode', 'keys-only',
                target,
                arch
            ])

        # Move generated stuff out of the way from defsdir
        for basename in [ resultname, 'ybd.result', 'ybd.environment' ]:
            shutil.move(os.path.join(defsdir, basename), workdir)

    return os.path.join(workdir, resultname)
